package cn.spring.goods.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cn.spring.goods.service.GoodsService;

@Controller("GoodsController")
@RequestMapping("/")
public class GoodsController {

	@Autowired
	GoodsService goodsService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index(Model model) {
		// return "helloWorld";
		return "index";
	}
}
